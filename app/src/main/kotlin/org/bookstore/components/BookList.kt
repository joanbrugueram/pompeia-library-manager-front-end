package org.bookstore.components

import io.kvision.core.*
import io.kvision.html.*
import io.kvision.panel.VPanel
import io.kvision.state.ObservableList
import io.kvision.state.bind
import io.kvision.state.bindEach
import io.kvision.utils.perc
import io.kvision.utils.px
import org.bookstore.model.BookModel

class BookList(private val bookList: ObservableList<BookModel>) : VPanel() {
    init {
        val thStyle = Style {
            Border(width = 1.px, color = Color(Col.BLACK.toString()), style = BorderStyle.SOLID)
        }

        div {
            h1("Book list")

            div().bind(bookList) { state ->
                if (state.isEmpty()) {
                    span("There are no books in the list")
                    return@bind
                }

                table {
                    textAlign = TextAlign.CENTER
                    width = 100.perc
                    thead {
                        tr {
                            th { addCssStyle(thStyle); width = 10.perc; +"ISBN" }
                            th { addCssStyle(thStyle); width = 40.perc; +"Title" }
                            th { addCssStyle(thStyle); width = 40.perc; +"Authors" }
                            th { addCssStyle(thStyle); width = 10.perc; +"Actions" }
                        }
                    }

                    tbody {
                        bindEach(bookList) { state ->
                            tr {
                                td { +state.isbn }
                                td { +state.title }
                                td { +state.authors.toString() }
                                td {
                                    add(Button("❌", style = ButtonStyle.PRIMARY).apply {
                                        onClick {
                                            bookList.remove(state)
                                        }
                                    })
                                }
                            }
                        }
                    }

                }
            }
        }
    }
}
