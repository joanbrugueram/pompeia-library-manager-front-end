import io.kvision.core.onEvent
import io.kvision.form.formPanel
import io.kvision.form.text.Text
import io.kvision.html.ButtonStyle
import io.kvision.html.ButtonType
import io.kvision.html.button
import io.kvision.html.div
import io.kvision.panel.SimplePanel
import io.kvision.utils.px
import org.bookstore.model.AuthorList
import org.bookstore.model.AuthorListSerializer
import org.bookstore.model.BookModel

class BookFormPanel(private val callback: (model: BookModel) -> Unit) : SimplePanel() {
    init {
        this.marginTop = 10.px

        div {
            formPanel<BookModel>(customSerializers = mapOf(AuthorList::class to AuthorListSerializer)) {
                add(BookModel::isbn, Text(label = "ISBN:"), required = true)
                add(BookModel::title, Text(label = "Title:"), required = true)
                addCustom(BookModel::authors, Text(label = "Authors (comma-separated):"), required = true)

                onEvent {
                    submit = {
                        it.preventDefault()
                        if (validate()) {
                            val model = getData()
                            console.log("Model: ${JSON.stringify(model)}")
                            callback(model)
                            setData(BookModel("", "", AuthorList(mutableListOf())))
                        }
                    }
                }

                button("Submit", type = ButtonType.SUBMIT, style = ButtonStyle.PRIMARY)
            }
        }
    }
}