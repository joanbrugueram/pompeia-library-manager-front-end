package org.bookstore.model

import kotlinx.serialization.KSerializer
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.descriptors.buildClassSerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder

class AuthorList(private val list: List<String>) {
    override fun toString(): String {
        return list.joinToString(", ")
    }
}

object AuthorListSerializer : KSerializer<AuthorList> {
    override val descriptor: SerialDescriptor = buildClassSerialDescriptor("org.bookstore.AuthorList")

    override fun deserialize(decoder: Decoder): AuthorList {
        val str = decoder.decodeString()
        return AuthorList(str.split(",").map(String::trim))
    }
    override fun serialize(encoder: Encoder, value: AuthorList) {
        encoder.encodeString(value.toString())
    }
}
