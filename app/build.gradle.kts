plugins {
    kotlin("js")
    kotlin("plugin.serialization")
    id("io.kvision") version libs.versions.kvision.get()
}

version = "0.0.0-SNAPSHOT"

val kvisionVersion: String = libs.versions.kvision.get()

kotlin {
    js {
        browser()
        binaries.executable()
    }
}

dependencies {
    implementation("io.kvision:kvision:$kvisionVersion")
    implementation("io.kvision:kvision-state:$kvisionVersion")

    testImplementation(kotlin("test-js"))
    testImplementation("io.kvision:kvision-testutils:$kvisionVersion")
}
