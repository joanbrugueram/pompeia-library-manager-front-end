package org.bookstore

import BookFormPanel
import io.kvision.Application
import io.kvision.CoreModule
import io.kvision.html.*
import io.kvision.module
import io.kvision.panel.root
import io.kvision.startApplication
import io.kvision.state.*
import org.bookstore.components.BookList
import org.bookstore.model.AuthorList
import org.bookstore.model.BookModel


class App : Application() {

    private val bookList = observableListOf(
            BookModel("9780553803709", "Dune", AuthorList(listOf("Frank Herbert"))),
            BookModel("9780062457714", "To Kill a Mockingbird", AuthorList(listOf("Harper Lee"))),
            BookModel("9781400079988", "1984", AuthorList(listOf("George Orwell"))),
            BookModel("9780061120084", "Pride and Prejudice", AuthorList(listOf("Jane Austen"))),
            BookModel("9780060935467", "The Catcher in the Rye", AuthorList(listOf("J.D. Salinger"))),
            BookModel("9780765348273", "Good Omens", AuthorList(listOf("Neil Gaiman", "Terry Pratchett"))),
    )

    override fun start() {
        root("kvapp") {
            add(BookList(bookList))

            h1("Add a new book")
            add(BookFormPanel { bookModel ->
                bookList.add(bookModel)
            })
        }
    }
}

fun main() {
    startApplication(::App, module.hot, CoreModule)
}