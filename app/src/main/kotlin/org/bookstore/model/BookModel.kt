package org.bookstore.model

import kotlinx.serialization.Contextual
import kotlinx.serialization.Serializable

@Serializable
data class BookModel(val isbn: String, val title: String, @Contextual val authors: AuthorList)
